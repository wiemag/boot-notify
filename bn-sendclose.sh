#!/bin/bash
version=0.95 # by wm, 2023-11-01
# Dependencies:  See bn-sendopen.sh.
debug=1

RECIPIENT="${1-$USER}"
# If $RECIPIENT is not a qualified domain address,
# look for the address in /etc/aliases.
if [[ "$RECIPIENT" == "${RECIPIENT%@*}" ]]
then
	if [[ -r /etc/aliases ]]
	then
		RECIPIENT="$(awk '/^'${RECIPIENT}':/ {print $2;exit}' /etc/aliases)"
		RECIPIENT="${RECIPIENT%%,*}"
		[[ -z "$RECIPIENT" ]] && exit 4  # E-mail address not found.
	else
		exit 6  # /etc/aliases does not exist.
	fi
else
	[[ "$RECIPIENT" == "${RECIPIENT%@*.*}" ]] && exit 5  # Wrong e-mail address.
fi

if echo "Shutdown." | mail -s "Shutdown notification from $HOSTNAME" "$RECIPIENT"
then
	: # Wait until the $? is known
else
	((debug)) && echo Failed.
fi
sleep 3 # Give the system extra time to wrap things up.
