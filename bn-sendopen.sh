#!/bin/bash
version=0.95 # 2023-11-01
sleep 2 # give system the time to set up network
debug=0
# By Wiesław Magusiak, 2013-10-27, 2015-12-13 wersion 0.93
# Sends sender's external and local IP's and the active network interface.

# DEPENDENCIES
# bind (dig), iproute2 (ip), s-nail (mail)

function myIPs () {
printf "%s\n%s\n%s" $(dig +short myip.opendns.com @resolver1.opendns.com) $(ip route|awk '/defau/ {print $9" "$5}')
}

while getopts  "s:dvh" flag; do	# Read e-mail Subject, if any.
  case "$flag" in
  h|\?) echo "Read the man pages:  man boot-notify";
	  exit 0;;
  d)  debug=1;;
  s)  SUBJECT="$OPTARG"; break;;	# E-mail subject
  v)  echo ${0##*/}, version ${version}; exit;;
  esac
done
shift $((OPTIND - 1))
SUBJECT="${SUBJECT-Boot notification from $HOSTNAME}" 	# Default Subject
RECIPIENT="${1-$USER}"
# If $RECIPIENT is not a qualified domain address,
# look for the address in /etc/aliases.
if [[ "$RECIPIENT" == "${RECIPIENT%@*}" ]] # No '@' in the recipient name.
then
	if [[ -e /etc/aliases ]]; then
		RECIPIENT="$(awk '!/^#/ && /^'${RECIPIENT}':/ {print $2;exit}' /etc/aliases)"
		RECIPIENT="${RECIPIENT%%,*}"
		[[ -z "$RECIPIENT" ]] && exit 4 		# E-mail address not found.
	else
		exit 6 								# /etc/aliases does not exist.
	fi
else
	[[ "$RECIPIENT" == "${RECIPIENT%@*.*}" ]] && exit 5 	# Wrong e-mail address.
fi

((debug)) && echo -e "$(myIPs) | mail -s '$SUBJECT' $RECIPIENT"
echo -e "$(myIPs)" | mail -s "$SUBJECT" $RECIPIENT
